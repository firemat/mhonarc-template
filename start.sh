#!/usr/bin/env bash

confirm()
{
  read -r -p "${1} [y/N] " response

  case "$response" in
    [yY][eE][sS]|[yY]) 
      true
      ;;
    *)
      false
      ;;
  esac
}

if [ $# -ne 2 ]; then
    echo "The number of arguments passed is incorrect"
    exit 1
fi

mhonarc_exec="mhonarc"
if ! [ -x "$(command -v mhonarc)" ]; then
  echo "Warning: mhonarc is not installed. Please provide a path to the executable."
  read mhonarc_exec
fi

echo "-----------------------"
template="message.rc"
template_use=false
if confirm "Do you want to use a template ?"; then
	echo "Default template will be $template"
	template_use=true
else
  echo "No template will be used"
fi

dir_mail=$1
dir_out=$2


echo "-----------------------"
echo "Check existance of folder $dir_out"
if [ -d "$dir_out" ]; then
  echo "Success - $dir_out exists"
else
  echo "Fail - $dir_out does not exist"
  if confirm "Do you want to create it ?"; then
    mkdir "$dir_out"
    echo "Folder $dir_out created"
	else
    echo "MHonArc needs an existing folder, reexecute this script with an existing folder."
    exit 0
	fi
fi

# Sanity check since we will use the * operator
 if [ "${dir_mail: -1}" != "/" ]; then
 	dir_mail+="/"
fi

echo "-----------------------"
echo "Run MHonArc"

if $template_use
then
	$mhonarc_exec -rcfile "$template" -outdir ${dir_out} ${dir_mail}*
else
	$mhonarc_exec -outdir ${dir_out} ${dir_mail}*
fi
