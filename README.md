# MHonArc template association

This project is only to complete the [MHonArc](https://github.com/sympa-community/MHonArc) software.

Since an INSA Lyon student association gets a simple archive of previous mail on their mailing list from Sympa, this script and templates are there to help them and to provide an easy way to convert the mail archive into a proper set of html pages.

## Usage

Before using this script, you will need to install [MHonArc 2.6.24](https://github.com/sympa-community/MHonArc/releases/tag/2.6.24), and [Perl](https://www.perl.org/) (at least v5.0).

If needed, this is the set of command to install MHonArc when inside the folder of the decompressed MhonArc archive:
```
for i in lib/*.pl lib/*.pm lib/*/*.pm; do perl -Ilib -c $i; done
perl Makefile.PL
make
make install
```

Get the archive from sympa, and uncompress it to a folder. Here we will name it `archiveList` for example purpose.

Clone this repository, go to the project folder, and then execute the `start.sh` script
```
./start.sh <folder_input> <folder_output>
```
where:
- `<folder_input>` is the folder containing every folder archive from Sympa. This corresponds to the path to `archiveList` folder for example ;
- `<folder_output>` is the path of the destination folder of the HTML pages.

## Dependencies

This project uses:
- [Bootstrap 5.1](https://getbootstrap.com/docs/5.1/getting-started/introduction/) &mdash; to improve the style and feeling of the generated pages ;
- [Bootstrap Icons 1.7.1](https://icons.getbootstrap.com/) &mdash; to have some beautiful icons ;
- [MHonArc](https://www.mhonarc.org/) &mdash; to use our templates and transform the mail archive.

## Licence

[GNU GPL v2.0](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

```
MHonArc template association - A BootStrap template for MHonArc
Copyright (C) 2021 Matthieu Halunka

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
```
