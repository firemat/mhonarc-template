# MHonArc template for association changelog

The latest version of this file can be found at the main branch of the MHonArc template for association repository.

## 0.1

### Removed (0 change)

### Fixed (0 change, 0 of them are from the community)

### Changed (0 change)

### Added (4 changes)
- Adding templates for MHonArc
- Adding templates for GitLab issues/MR
- Adding basic project and code owners files
- Adding bash script to have an easier usage of MHonArc

### Other (0 change)
